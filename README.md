# Drupal Module: Collapse Bbq Patch
**Author:** Aaron Klump  <sourcecode@intheloftstudios.com>

## Summary
**This patches a core issue that precludes the use of jquery bbq and collapsible fieldsets on the same page.  For more info see https://www.drupal.org/node/2651230#comment-10754018.**

You may also visit the [project page](https://www.drupal.org/sandbox/aklump/2651232) on Drupal.org.

## Installation
1. Install as usual, see [http://drupal.org/node/70151](http://drupal.org/node/70151) for further information.

## Configuration
1. Simple turn it on, there is no configuration.

## Contact
* **In the Loft Studios**
* Aaron Klump - Developer
* PO Box 29294 Bellingham, WA 98228-1294
* _skype_: intheloftstudios
* _d.o_: aklump
* <http://www.InTheLoftStudios.com>
